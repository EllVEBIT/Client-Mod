# Client-Mod
[![Build Status](https://app.travis-ci.com/EllVEBIT/Client-Mod.svg?branch=main)](https://app.travis-ci.com/EllVEBIT/Client-Mod)

 * Commands:
  
cl_autojump - [1/0] Autojump on SPACEBAR key, default value is 1.
When this command is enabled the jump button may not work, solved by command cl_lw 1. Taken from mobile port ag6.6, autojump author Nekonomicon. 
  
hud_color "r g b" - Sets hud color, values must be enclosed in quotation marks. example: hud_color "255 0 255".

hud_weapon - [1/0] Draw current weapon in HUD, default value is 1.

hud_new - [1/0] The color of armor and health indicators will change depending on their number, default value is 0.

cl_lowerweapon - [1/0] The weapon in the player's hand will lower depending on the speed of movement, default value is 0.

cl_gausscolor - [1/0] activates cvar cl_gaussbeam "r g b", default value is 0.

hud_speedometer - [1/0] shows the player's speed, default value is 1. 

cl_colored_speed -[1/0] activates cvar hud_speedcolor "r g b" - draw custom speedometer color, default value is 0.